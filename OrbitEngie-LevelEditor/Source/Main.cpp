#include "Editor/AppStates/MainEditorState.h"

int main()
{

	orb::Window window(1850, 1000, "Orbit-Engie     Level Editor");
	window.setPosition(sf::Vector2i(0, 0));
	//window.setFramerateLimit(60);

	orb::editor::MainEditorState app(&window);	//TODO: rename panes to roots
	app.run();									//TODO: shader als background
												//TODO: tooltip on hover bij map view, zoals pos, vertecies, etc
}