#include "GameState.h"

orb::GameState::GameState(Window* window)
{
	m_window = window;
}

orb::GameState::~GameState()
{
	for (int i = 0; i < m_roots.size(); i++)
	{
		delete m_roots[i];
	}

	m_roots.clear();
}

void orb::GameState::addGuiRoot(GuiRoot * window)
{
	m_roots.push_back(window);
}

void orb::GameState::run()
{
	while (isActive() && m_window->isActive())
	{
		m_window->prepare();
		m_eventHandle.handleAllEvents(m_window);

		preUpdate();

		for (int i = 0; i < m_roots.size(); i++)
			m_roots[i]->draw();

		m_window->update();
	}
}
