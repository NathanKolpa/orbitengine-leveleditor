#pragma once
#include "../Window/Window.h"
#include "../Input/EventHandle.h"

#include "../Input/ApplicationEvent.h"
#include "../Input/KeyboardEvent.h"
#include "../Input/MouseButtonEvent.h"
#include "../Input/MouseEvent.h"

#include "../GUI/GuiRoot.h"

namespace orb
{
	class GameState
	{
	private:
		std::vector<GuiRoot*> m_roots;
	protected:
		Window* m_window;
		EventHandle m_eventHandle;
	public:
		GameState(Window* window);
		~GameState();
	protected:
		void addGuiRoot(GuiRoot* root);
		virtual bool isActive() { return false; }
		virtual void preUpdate() {}
	public:
		void run();
	};
}