#include "Resource.h"
#include <iostream>

orb::Resource::Resource(std::string folder)
{
	m_folder = folder;
}

orb::Resource::~Resource()
{
}

std::string orb::Resource::getPath()
{
	return m_beginPath + m_folder + "/";
}

void orb::Resource::errorFailToLoad(std::string filePath)
{
	std::cout << "failed to load resource: " << filePath << std::endl;
}

bool orb::Resource::isLoaded()
{
	return loaded;
}
