#include "FontResource.h"
#include <fstream>
#include <iostream>
#include <iomanip>

orb::FontResource::FontResource()
	:Resource("Fonts")
{
}

orb::FontResource::FontResource(std::string file)
	:Resource("Fonts")
{
	load(file);
}

orb::FontResource::~FontResource()
{
}

bool orb::FontResource::loadFont(std::string file)
{
	if (!m_font.loadFromFile(getPath() + file))
	{
		loadDefault();

		return false;
	}
	return true;
}

bool orb::FontResource::load(std::string file)
{
	loaded = true;

	return loadFont(file);
}

void orb::FontResource::loadDefault()
{
	loaded = true;
	m_font.loadFromFile("C:/Windows/Fonts/arial.ttf");
}

sf::Font & orb::FontResource::getFont()
{
	return m_font;
}
