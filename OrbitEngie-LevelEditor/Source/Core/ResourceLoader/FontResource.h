#pragma once
#include "Resource.h"

namespace orb
{
	class FontResource : public Resource
	{
	private:
		sf::Font m_font;
	public:
		FontResource();
		FontResource(std::string file);
		~FontResource();
	private:
		bool loadFont(std::string file);
	public:
		bool load(std::string file);
		void loadDefault();
		sf::Font& getFont();
	};

}