#pragma once
#include "Resource.h"

namespace orb
{
		class TextureResource : public Resource
		{
		private:
			sf::Texture m_texture;
		public:
			TextureResource();
			TextureResource(std::string file);
			~TextureResource();
		private:
			bool loadTexture(std::string file);
		public:
			void loadDefault();
			bool load(std::string file);
			sf::Texture& getTexture();
		};
	
}