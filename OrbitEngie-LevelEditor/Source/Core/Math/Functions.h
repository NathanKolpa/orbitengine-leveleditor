#pragma once
#include <cmath>

namespace orb
{
	template<typename T>
	bool isPow2(T n)
	{
		return (n & (n - 1)) == 0;
	}

	template<typename T>
	static T getPrevPow2(T n);

	template<typename T>
	static T getNextPow2(T n)
	{
		if (n == 0)
			return 1;

		if (n < 0)
		{
			return -getPrevPow2(abs(n));
		}

		T value = 1;
		while (value <= n)
		{
			value = value << 1;
		}
		return value;
	}


	template<typename T>
	static T getPrevPow2(T n)
	{
		if (n == 0)
			return -1;


		if (n < 0)
		{
			return -getNextPow2(abs(n));
		}

		if ((n && !(n&(n - 1))) == 1)
		{
			return (n >> 1);
		}

		while (n&n - 1)
		{
			n = n & n - 1;
		}
		return  n;
	}
}