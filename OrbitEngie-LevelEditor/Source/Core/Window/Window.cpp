#include "Window.h"
#include "Time.h"
#include <ImGUI/imgui-SFML.h>
#include <ImGUI/imgui.h>

//hiding window
#if defined(_DEBUG)

#define ORB_CONSOLE_SHOWN
#include <iostream>

void hideWindow()
{
	std::cout << "[debug console]" << std::endl;
}

void showWindow()
{
}

#else

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)

#define ORB_CONSOLE_HIDDEN

#include <Windows.h>
void hideWindow()
{
	HWND hWnd = GetConsoleWindow();
	ShowWindow(hWnd, SW_HIDE);
}

void showWindow()
{
	HWND hWnd = GetConsoleWindow();
	ShowWindow(hWnd, SW_SHOW);
}
#else
void hideWindow()
{
	HWND hWnd = GetConsoleWindow();
	ShowWindow(hWnd, SW_HIDE);
}

void showWindow()
{
	HWND hWnd = GetConsoleWindow();
	ShowWindow(hWnd, SW_SHOW);
}
#endif

#endif





int orb::Window::getWidth()
{
	return m_windowWidth;
}

int orb::Window::getHeight()
{
	return m_windowHeight;
}

double orb::Window::getDeltaTime()
{
	return m_deltaTime;
}

/*
* initialize opengl and create a window
*/
orb::Window::Window(int width, int height, std::string name)
	:RenderWindow(sf::VideoMode(width, height), name)
{
	hideWindow();
	setKeyRepeatEnabled(true);

	ImGui::SFML::Init(*dynamic_cast<sf::RenderWindow*>(this));


	//the rest of the contructors
	m_isActive = true;
	m_windowWidth = width;
	m_windowHeight = height;
	m_windowName = name;

	Time::setDeltaTimePointer(&m_deltaTime);
}

orb::Window::~Window()
{
	showWindow();
}

void orb::Window::handleTime()
{
	m_previousTime = m_currentTime;
	m_currentTime = std::chrono::steady_clock::now();
	auto duration = m_currentTime - m_previousTime;

	auto secs = std::chrono::duration_cast<std::chrono::duration<double>>(duration);
	m_deltaTime = secs.count();

	_framecount++;
	_elapsedTime += m_deltaTime;
	_totalfps += Time::framesPerSecond();

	if (_elapsedTime > 0.25)
	{

		std::string newTitle = m_windowName + " - FPS: " + std::to_string(_totalfps / _framecount);
		setTitle(newTitle.c_str());

		_elapsedTime = 0;
		_totalfps = 0;
		_framecount = 0;
	}
}


/*
* dictates if the gameloop should be running
*/
bool orb::Window::isActive()
{
	return isOpen() && m_isActive;
}


void orb::Window::prepare()
{
	pushGLStates();
	handleTime();
	ImGui::SFML::Update(*dynamic_cast<sf::RenderWindow*>(this), m_deltaClock.restart());


	//clear(sf::Color(20, 20, 40));
	//clear(sf::Color(255, 255, 255));
	clear();
}

void orb::Window::update()
{
	ImGui::SFML::Render(*dynamic_cast<sf::RenderWindow*>(this));
	display();
	popGLStates();
}
