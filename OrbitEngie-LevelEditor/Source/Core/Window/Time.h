#pragma once
namespace orb
{
	class Time
	{
	private:
		static double* m_deltaTimePointer;
	public:
		static void setDeltaTimePointer(double* deltaTime);
		static double deltaTime();
		static int framesPerSecond();
	};
}