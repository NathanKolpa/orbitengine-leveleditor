#pragma once
#include "../GuiNode.h"
#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

namespace orb
{
	class GLNode : public GuiNode
	{
	private:
		sf::RenderTexture m_renderTexture;
		sf::Sprite m_sprite;
		bool isFirstFrame = true;
		std::function<void()> m_lamda;
	protected:
		int m_width, m_height;
	public:
		GLNode(int width, int height);
		~GLNode();
	protected:
		virtual void drawGL();
	public:
		void update();
		void draw();
		void setOnClickEvent(std::function<void()> lamda);
	};
}