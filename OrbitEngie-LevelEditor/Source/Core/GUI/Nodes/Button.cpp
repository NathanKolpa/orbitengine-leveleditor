#include "Button.h"

orb::Button::Button(std::string label, ImVec2 size)
	:m_label(generateLabel(label)), m_size(size)
{
}

void orb::Button::setOnAction(std::function<void()> event)
{
	m_lamda = event;
}

void orb::Button::draw()
{
	pushDraw();
	if (ImGui::Button(m_label.c_str(), m_size))
	{
		if (m_lamda)
		{
			m_lamda();
		}
	}
	popDraw();
}
