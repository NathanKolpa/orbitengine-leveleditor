#include "GLNode.h"
#include <Log.h>
#include <SFML/OpenGL.hpp>
#include <SFML/Graphics/Rect.hpp>

orb::GLNode::GLNode(int width, int height)
	:m_width(width), m_height(height)
{
	if (!m_renderTexture.create(m_width, m_height))
	{
		std::cerr << "could not create a RenderTexture! reason: the width or height is invalid. (" << m_width << ", " << m_height << ")" << std::endl;
		return;
	}

	m_renderTexture.setActive(true);
	GLfnc(glViewport(0, 0, m_width, m_height));
	m_sprite.setTexture(m_renderTexture.getTexture());
	m_renderTexture.setActive(false);
	m_renderTexture.setSmooth(false);

}

orb::GLNode::~GLNode()
{
}

void orb::GLNode::drawGL()
{
	//by default draw a triangle
	glBegin(GL_TRIANGLES);
	glVertex2f(0, 1);
	glVertex2f(-1, -1);
	glVertex2f(1, -1);
	glEnd();

}

void orb::GLNode::draw()
{
	if (isFirstFrame)
	{
		isFirstFrame = false;
		update();
	}

	pushDraw();
	ImGui::Image(m_sprite, sf::Color::White, sf::Color(50, 50, 50));
	if (ImGui::IsItemClicked())
	{
		if (m_lamda)
			m_lamda();
	}
	popDraw();

}

void orb::GLNode::update()
{
	m_renderTexture.pushGLStates();
#ifdef _DEBUG
	if (!m_renderTexture.setActive(true))
	{
		std::cerr << "could not set a RenderTexture active!" << std::endl;
	}
#else
	m_renderTexture.setActive(true);
#endif
	m_renderTexture.clear();

	//OPENGL RENDERING
	GLfnc(glViewport(0, 0, m_width, m_height));//VIEWPORT MIGHT CAUSE ISSIUES
	GLfnc(glScalef(1, -1, 1));

	drawGL();

	GLfnc(glFlush());
	//END OPENGL RENDERING
	m_renderTexture.display();
#ifdef _DEBUG
	if (!m_renderTexture.setActive(false))
	{
		std::cerr << "could not set a RenderTexture unactive!" << std::endl;
	}
#else
	m_renderTexture.setActive(false);
#endif
	m_renderTexture.popGLStates();
}

void orb::GLNode::setOnClickEvent(std::function<void()> lamda)
{
	m_lamda = lamda;
}
