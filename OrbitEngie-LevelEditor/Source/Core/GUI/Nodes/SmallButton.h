#pragma once
#include "Button.h"

namespace orb
{
	class SmallButton : public Button
	{
	public:
		SmallButton(std::string label)
			:Button(label)
		{
		}

		void draw()
		{
			pushDraw();
			if (ImGui::SmallButton(m_label.c_str()))
			{
				if (m_lamda)
					m_lamda();

			}
			popDraw();
		}
	};
}