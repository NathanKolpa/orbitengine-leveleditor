#pragma once
#include "../GuiNode.h"
namespace orb
{
	class HelpMarker : public GuiNode
	{
	private:
		std::string m_text;
	public:
		HelpMarker(std::string text = "")
			:m_text(text)
		{

		}

		void draw()
		{
			pushDraw();
			ImGui::TextDisabled("(?)");
			if (ImGui::IsItemHovered())
			{
				ImGui::BeginTooltip();
				ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
				ImGui::TextUnformatted(m_text.c_str());
				ImGui::PopTextWrapPos();
				ImGui::EndTooltip();
			}
			popDraw();
		}
	};
}