#pragma once
#include "../Children.h"

namespace orb
{
	class CollapsingHeader : public GuiNode
	{
	private:
		Children<GuiNode> m_children;
		std::string m_label;
	public:
		CollapsingHeader(std::string label)
			:m_label(label)
		{

		}

		Children<GuiNode>& getChildren()
		{
			return m_children;
		}

		void draw()
		{
			ImGui::SetNextTreeNodeOpen(true, ImGuiCond_FirstUseEver);			
			pushDraw();
			if (ImGui::CollapsingHeader(m_label.c_str()))
			{
				m_children.drawAll();
			}
			popDraw();
		}
	};
}