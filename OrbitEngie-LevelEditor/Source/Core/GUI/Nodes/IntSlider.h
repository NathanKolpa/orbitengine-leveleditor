#pragma once
#include "../GuiNode.h"

namespace orb
{
	class IntSlider : public GuiNode
	{
	private:
		int m_currentValue = 0;
		int m_lastValue = 0;
		int m_min, m_max;
		std::string m_label;
		std::function<void(int lastValue, int newValue)> m_lamdba;
	public:
		IntSlider(std::string label, int min, int max)
			:m_label(generateLabel(label)), m_min(min), m_max(max)
		{
			m_currentValue = min;
		}

		void setValue(int value)
		{
			m_currentValue = value;
			if (m_lamdba)
				m_lamdba(m_lastValue, m_currentValue);
			m_lastValue = value;

		}

		int getMax()
		{
			return m_max;
		}


		int getMin()
		{
			return m_min;
		}

		int getValue()
		{
			return m_currentValue;
		}

		void draw()
		{
			pushDraw();
			ImGui::SliderInt(m_label.c_str(), &m_currentValue, m_min, m_max);
			popDraw();

			if (m_lastValue != m_currentValue)
			{
				if (m_lamdba)
					m_lamdba(m_lastValue, m_currentValue);
			}

			m_lastValue = m_currentValue;
		}

		void setOnValueChange(std::function<void(int lastValue, int newValue)> lamdba)
		{
			m_lamdba = lamdba;
		}
	};
}