#pragma once
#include "../GuiNode.h"

namespace orb
{
	class Button : public GuiNode
	{
	protected:
		std::function<void()> m_lamda;
		std::string m_label;
		int m_id;
		ImVec2 m_size;
	public:
		Button(std::string label = "", ImVec2 size = ImVec2(0, 0));
	public:
		void setOnAction(std::function<void()> event);
		void draw();
	};
}