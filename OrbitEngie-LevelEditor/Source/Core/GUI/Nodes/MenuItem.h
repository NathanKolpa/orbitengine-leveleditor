#pragma once
#include "../GuiNode.h"

namespace orb
{
	class MenuItem : public GuiNode
	{
	private:
		std::string m_label;
		std::function<void()> m_lamda;
		bool m_isChecked = false;
	public:
		MenuItem(std::string label)
			:m_label(label)
		{

		}

		void setOnAction(std::function<void()> lamda)
		{
			m_lamda = lamda;
		}

		void setCheckmark(bool flag)
		{
			m_isChecked = flag;
		}

		void draw()
		{
			pushDraw();
			if (ImGui::MenuItem(m_label.c_str(), NULL, m_isChecked))
			{
				if (m_lamda)
					m_lamda();
			}
			popDraw();
		}
	};
}