#include "GuiNode.h"

int orb::GuiNode::s_idStack;
int orb::GuiNode::s_count = 0;

orb::GuiNode::GuiNode()
{
	s_count++;
}

orb::GuiNode::~GuiNode()
{
	s_count--;
}

void orb::GuiNode::pushDraw()
{
	ImGui::PushID(s_idStack);
	s_idStack++;
	if (m_sameLine)
		ImGui::SameLine();
	for (int i = 0; i < m_colorStack.size(); i++)
	{
		ImGui::PushStyleColor(m_colorStack[i].flag, m_colorStack[i].color);
	}
}

void orb::GuiNode::popDraw()
{
	ImGui::PopStyleColor((int)m_colorStack.size());
	ImGui::PopID();
	s_idStack--;
}

std::string orb::GuiNode::generateLabel(std::string label)
{
	return label + "##" + std::to_string(s_count);
}

void orb::GuiNode::draw()
{
}

void orb::GuiNode::setSameLine(bool flag)
{
	m_sameLine = flag;
}

void orb::GuiNode::pushColor(ImGuiCol flag, ImVec4 color)
{
	guicolor temp;
	temp.color = color;
	temp.flag = flag;
	m_colorStack.push_back(temp);
}
