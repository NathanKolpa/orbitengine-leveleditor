#include "GuiWindow.h"
#include <iostream>

float orb::GuiWindow::s_windowPos;

orb::GuiWindow::GuiWindow(std::string label, int width)
	:m_label(label), m_prefWidth(width)
{
	m_windowPos = s_windowPos;
	s_windowPos += width;
}

orb::GuiWindow::~GuiWindow()
{
}

void orb::GuiWindow::setEnable(bool flag)
{
	m_isOpen = flag;
}

bool orb::GuiWindow::isOpen()
{
	return m_isOpen;
}

std::string orb::GuiWindow::getLabel()
{
	return m_label;
}

void orb::GuiWindow::setOnResize(std::function<void(int nWidth, int nHeight)> lamda)
{
	m_resizeLamda = lamda;
}

void orb::GuiWindow::draw()
{
	if (m_isOpen)
	{
		ImGui::SetNextWindowSize(ImVec2((float)m_prefWidth, 100.0f), ImGuiCond_FirstUseEver);
		ImGui::SetNextWindowPos(ImVec2(m_windowPos, 20), ImGuiCond_FirstUseEver);
		if (ImGui::Begin(m_label.c_str(), &m_isOpen))
		{
			m_children.drawAll();
		}

		ImVec2 current = ImGui::GetWindowSize();

		if (m_lastSize.x != current.x || m_lastSize.y != current.y)
		{
			if (m_resizeLamda)
				m_resizeLamda(current.x, current.y);
		}

		m_lastSize = current;

		ImGui::End();
	}
}

orb::Children<orb::GuiNode>& orb::GuiWindow::getChildren()
{
	return m_children;
}