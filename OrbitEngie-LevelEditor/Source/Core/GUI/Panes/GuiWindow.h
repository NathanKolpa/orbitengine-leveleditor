#pragma once
#include "../GuiRoot.h"

namespace orb
{
	class GuiWindow : public GuiRoot
	{
	private:
		std::string m_label;
		Children<GuiNode> m_children;
		static float s_windowPos;
		float m_windowPos = 0;
		int m_prefWidth;
		bool m_isOpen = true;
		ImVec2 m_lastSize;
		std::function<void(int nWidth, int nHeight)> m_resizeLamda;
	public:
		GuiWindow(std::string label, int width);
		~GuiWindow();
	public:
		void draw();
		void setEnable(bool flag);
		bool isOpen();
		std::string getLabel();
		void setOnResize(std::function<void(int nWidth, int nHeight)> lamda);
		Children<GuiNode>& getChildren();
	};
}
