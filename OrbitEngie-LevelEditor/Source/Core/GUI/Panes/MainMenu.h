#pragma once
#include "../GuiRoot.h"

namespace orb
{
	class MenuList : public GuiRoot
	{
	private:
		std::string m_label;
		Children<GuiNode> m_children;
	public:
		MenuList(std::string label)
			:m_label(label)
		{

		}

		Children<GuiNode>& getChildren()
		{
			return m_children;
		}

		virtual void draw()
		{
			if (ImGui::BeginMenu(m_label.c_str()))
			{
				m_children.drawAll();

				ImGui::EndMenu();
			}
		}
	};

	class MainMenu : public GuiRoot
	{
	private:
		Children<MenuList> m_items;
	public:
		MainMenu()
		{

		}

		Children<MenuList>& getChildren()
		{
			return m_items;
		}

		void draw()
		{
			ImGui::BeginMainMenuBar();

			m_items.drawAll();

			ImGui::EndMainMenuBar();
		}
	};
}