#pragma once
#include <vector>
#include "GuiNode.h"

namespace orb
{
	template<typename T>
	class Children
	{
	private:
		std::vector<T*> m_children;
	public:
		~Children()
		{
			for (int i = 0; i < m_children.size(); i++)
			{
				if (m_children[i])
					delete m_children[i];
			}

			m_children.clear();
		}

		void add(T* node)
		{
			m_children.push_back(node);
		}

		template<class ... Args>
		void addAll(Args ... args)
		{
			T* list[] = { (args)... };

			for (int i = 0; i < sizeof(list) / sizeof(T*); i++)
				add(list[i]);
		}

		void drawAll()
		{
			for (int i = 0; i < m_children.size(); i++)
				m_children[i]->draw();
		}
	};
}