#pragma once
#include <string>
#include <ImGUI/imgui-SFML.h>
#include <ImGUI/imgui.h>
#include <functional>
#include <vector>

struct guicolor
{
	ImGuiCol flag;
	ImVec4 color;
};

namespace orb
{
	class GuiNode
	{
	private:
		bool m_sameLine = false;
		std::vector<guicolor> m_colorStack;
		static int s_idStack;
		static int s_count;
	public:
		GuiNode();
		~GuiNode();
	protected:
		void pushDraw();
		void popDraw();
		std::string generateLabel(std::string label);
	public:
		virtual void draw();
		void setSameLine(bool flag);
		void pushColor(ImGuiCol flag, ImVec4 color);
	};
}