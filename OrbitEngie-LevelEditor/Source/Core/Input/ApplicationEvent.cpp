#include "ApplicationEvent.h"

orb::ApplicationEvent::ApplicationEvent(sf::RenderWindow* window)
{
	m_window = window;
}

orb::ApplicationEvent::~ApplicationEvent()
{
}

//enable disable means focus
bool orb::ApplicationEvent::handleState(sf::Event event)
{
	switch (event.type)
	{
	case sf::Event::Closed:
		if (m_closeCallback != nullptr)
			m_closeCallback(m_window);
		return true;

	case sf::Event::Resized:
		if (m_resizeCallback != nullptr)
			m_resizeCallback(event.size.width, event.size.height);
		return true;
	}

	return false;
}

void orb::ApplicationEvent::setOnCloseEvent(std::function<void(sf::RenderWindow*)> callback)
{
	m_closeCallback = callback;
}

void orb::ApplicationEvent::setOnWindowResizeEvent(std::function<void(int, int)> callback)
{
	m_resizeCallback = callback;
}
