#pragma once
#include <functional>
#include <SFML/Graphics.hpp>

namespace orb
{
	class Event
	{
	protected:
		bool lastState = false;
		bool currentState = false;
	public:
		Event();
		~Event();
	protected:
		virtual bool handleState(sf::Event event) { return false; }
	public:
		bool handle(sf::Event event);
	};
}