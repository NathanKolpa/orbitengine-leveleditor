#include "MouseEvent.h"

orb::MouseEvent::MouseEvent(sf::RenderWindow* window)
{
	m_window = window;
}

orb::MouseEvent::~MouseEvent()
{
}

bool orb::MouseEvent::handleState(sf::Event event)
{
	sf::Vector2i current = sf::Mouse::getPosition(*m_window);

	if (current != m_lastMouse)
	{//the mouse moved
		if (m_mouseMoveCallback != nullptr)
			m_mouseMoveCallback(current.x, current.y);
	}

	m_lastMouse = current;

	return current != m_lastMouse ? true : false;
}

void orb::MouseEvent::setOnMouseMoveEvent(std::function<void(int, int)> callback)
{
	m_mouseMoveCallback = callback;
}
