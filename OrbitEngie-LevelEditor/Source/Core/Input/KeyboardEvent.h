#pragma once
#include "Event.h"

namespace orb
{
	class KeyboardEvent : public Event
	{
	private:
		sf::Keyboard::Key m_keycode;
		std::function<void()> m_holdCallback, m_pressCallback, m_releaseCallback;
	public:
		KeyboardEvent(sf::Keyboard::Key keycode);
		~KeyboardEvent();
	private:
		bool handleState(sf::Event event);
	public:
		void setOnHoldCallback(std::function<void()> callback);
		void setOnPressCallback(std::function<void()> callback);
		void setOnReleaseCallback(std::function<void()> callback);
	};
}