#include "Event.h"

orb::Event::Event()
{
}

orb::Event::~Event()
{
}

bool orb::Event::handle(sf::Event event)
{
	bool ans = handleState(event);

	lastState = currentState;

	return ans;
}
