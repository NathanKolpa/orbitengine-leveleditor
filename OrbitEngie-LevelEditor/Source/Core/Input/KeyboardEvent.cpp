#include "KeyboardEvent.h"

orb::KeyboardEvent::KeyboardEvent(sf::Keyboard::Key keycode)
{
	m_keycode = keycode;
}

orb::KeyboardEvent::~KeyboardEvent()
{
}

bool orb::KeyboardEvent::handleState(sf::Event event)
{
	if (sf::Keyboard::isKeyPressed(m_keycode))
	{
		if (m_holdCallback != nullptr)
			m_holdCallback();

		currentState = true;
	}
	else
	{
		currentState = false;
	}

	if (currentState)
	{
		if (!lastState && m_pressCallback != nullptr)
		{
			m_pressCallback();
		}
	}
	else
	{
		if (lastState && m_releaseCallback != nullptr)
		{
			m_releaseCallback();
		}
	}

	return currentState;
}

void orb::KeyboardEvent::setOnHoldCallback(std::function<void()> callback)
{
	m_holdCallback = callback;
}

void orb::KeyboardEvent::setOnPressCallback(std::function<void()> callback)
{
	m_pressCallback = callback;
}

void orb::KeyboardEvent::setOnReleaseCallback(std::function<void()> callback)
{
	m_releaseCallback = callback;
}
