#pragma once
#include "Event.h"

namespace orb
{
	class MouseButtonEvent : public Event
	{
	private:
		sf::Mouse::Button m_button;
		std::function<void()> m_holdCallback, m_clickCallback, m_releaseCallback;
	public:
		MouseButtonEvent(sf::Mouse::Button button);
		~MouseButtonEvent();
	private:
		bool handleState(sf::Event event);
	public:
		void setOnHoldCallback(std::function<void()> callback);
		void setOnClickCallback(std::function<void()> callback);
		void setOnReleaseCallback(std::function<void()> callback);
		void setOnScrollCallback(std::function<void(float)> callback);
	};
}
