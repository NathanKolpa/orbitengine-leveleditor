#include "MouseButtonEvent.h"

orb::MouseButtonEvent::MouseButtonEvent(sf::Mouse::Button button)
{
	m_button = button;
}

orb::MouseButtonEvent::~MouseButtonEvent()
{
}

bool orb::MouseButtonEvent::handleState(sf::Event event)
{
	if (sf::Mouse::isButtonPressed(m_button))
	{
		currentState = true;

		if (m_holdCallback != nullptr)
			m_holdCallback();
	}
	else
	{
		currentState = false;
	}

	if (currentState)
	{
		if (!lastState && m_clickCallback != nullptr)
		{
			m_clickCallback();
		}
	}
	else
	{
		if (lastState && m_releaseCallback != nullptr)
		{
			m_releaseCallback();
		}
	}

	return currentState;
}

void orb::MouseButtonEvent::setOnHoldCallback(std::function<void()> callback)
{
	m_holdCallback = callback;
}

void orb::MouseButtonEvent::setOnClickCallback(std::function<void()> callback)
{
	m_clickCallback = callback;
}

void orb::MouseButtonEvent::setOnReleaseCallback(std::function<void()> callback)
{
	m_releaseCallback = callback;
}
