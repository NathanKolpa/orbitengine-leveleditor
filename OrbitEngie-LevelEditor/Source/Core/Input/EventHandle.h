#pragma once
#include "Event.h"
#include <vector>

namespace orb
{
	class EventHandle
	{
	private:
		std::vector<Event*> m_events;
	public:
		EventHandle();
		~EventHandle();
	public:
		void addEvent(Event* event);
		void handleAllEvents(sf::RenderWindow* window);
	};
}