#include "EventHandle.h"
#include <ImGUI/imgui-SFML.h>

orb::EventHandle::EventHandle()
{
}

orb::EventHandle::~EventHandle()
{
	for (int i = 0; i < (int)m_events.size(); i++)
	{
		delete m_events[i];
	}
}

void orb::EventHandle::addEvent(Event* event)
{
	m_events.push_back(event);
}

void orb::EventHandle::handleAllEvents(sf::RenderWindow* window)
{
	sf::Event current;
	while (window->pollEvent(current))
	{
		if (!ImGui::SFML::ProcessEvent(current))
		{
			for (int i = 0; i < (int)m_events.size(); i++)
			{
				if (m_events[i]->handle(current))
					break;
			}
		}
	}
}
