#pragma once
#include "../../Core/GUI/Nodes/GLNode.h"


namespace orb
{
	namespace editor
	{
		class View2D : public GLNode
		{
		private:
			static int s_gridSize;
			static float s_zoom;
			sf::Vector2i m_pos;
		public:
			View2D(int width, int height);
		private:
			void drawGL();
			void drawGrid();
			int transformX(double val);
			int transformY(double val);
		public:
			static void setGridSize(int size);
			static int getGridSize();
			static void setGridZoom(float zoom);
			static float getGridZoom();
			void setPosition(sf::Vector2i pos);
			sf::Vector2i getPosition();
		};
	}
}