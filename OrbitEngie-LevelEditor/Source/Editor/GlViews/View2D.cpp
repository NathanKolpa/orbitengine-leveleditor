#include "View2D.h"
#include <Log.h>
#include <climits>

int orb::editor::View2D::s_gridSize = 1;
float orb::editor::View2D::s_zoom = 1;

orb::editor::View2D::View2D(int width, int height)
	:GLNode(width, height)
{
}

void orb::editor::View2D::drawGL()
{
	GLfnc(glViewport(0, 0, m_width, m_height));
	GLfnc(glLoadIdentity());
	GLfnc(glOrtho(0, m_width, m_height, 0, 0, 1));
	GLfnc(glMatrixMode(GL_MODELVIEW));

	drawGrid();

	GLfnc(glFlush());
}

void orb::editor::View2D::drawGrid()
{

	GLfnc(glColor3f(0.2f, 0.2f, 0.2f));
	GLfnc(glLineWidth(1));
	glBegin(GL_LINES);//cannot use GLfnc with glvertex

	//right
	for (double x = 0; x < m_width + m_pos.x; x += getGridSize())
	{
		glVertex2d(transformX(x), 0);
		glVertex2d(transformX(x), m_height);
	}

	//left
	for (double x = 0; x < m_width - m_pos.x; x += getGridSize())
	{
		glVertex2d(transformX(-x), 0);
		glVertex2d(transformX(-x), m_height);
	}

	//HORIZONTAL

	//right
	for (double y = 0; y < m_height + m_pos.y; y += getGridSize())
	{
		glVertex2d(0, transformY(y));
		glVertex2d(m_width, transformY(y));
	}

	//left
	for (double y = 0; y < m_height - m_pos.y; y += getGridSize())
	{
		glVertex2d(0, transformY(-y));
		glVertex2d(m_width, transformY(-y));
	}


	glEnd();



	GLfnc(glLineWidth(1));
	GLfnc(glColor3d(0.9f, 0.0f, 0.0f));
	glBegin(GL_LINES);
	
	glVertex2i(transformX(8), transformY(0));
	glVertex2i(transformX(-8), transformY(0));

	glVertex2i(transformX(0), transformY(8));
	glVertex2i(transformX(0), transformY(-8));

	glEnd();

}

int orb::editor::View2D::transformX(double val)
{
	return ((val - m_pos.x) * getGridZoom()) + m_width / 2;
}

int orb::editor::View2D::transformY(double val)
{
	return ((val - m_pos.y) * getGridZoom()) + m_height / 2;
}




void orb::editor::View2D::setGridSize(int size)
{
	s_gridSize = size;
}

int orb::editor::View2D::getGridSize()
{
	return (s_gridSize);
}

void orb::editor::View2D::setGridZoom(float zoom)
{
	s_zoom = zoom;
}

float orb::editor::View2D::getGridZoom()
{
	return (s_zoom + 100) / 100;
}

void orb::editor::View2D::setPosition(sf::Vector2i pos)
{
	m_pos = pos;
}

sf::Vector2i orb::editor::View2D::getPosition()
{
	return m_pos;
}
