#include "MainEditorState.h"
#include <iostream>
#include "../../Core/Window/Time.h"

orb::editor::MainEditorState::MainEditorState(Window * window)
	:GameState(window)
{
	ImGui::StyleColorsClassic();
	//events
	ApplicationEvent* appEvents = new ApplicationEvent(m_window);
	appEvents->setOnCloseEvent([](sf::RenderWindow* lwindow)
	{
		lwindow->close();
	});
	m_eventHandle.addEvent(appEvents);


	KeyboardEvent* arrowUp = new  KeyboardEvent(sf::Keyboard::Up);
	arrowUp->setOnHoldCallback([&]()
	{
		if (m_focusedView)
		{
			m_focusedView->setPosition(sf::Vector2i(m_focusedView->getPosition().x, m_focusedView->getPosition().y + 16));
			m_focusedView->update();
		}
	});

	KeyboardEvent* arrowDown = new  KeyboardEvent(sf::Keyboard::Down);
	arrowDown->setOnHoldCallback([&]()
	{
		if (m_focusedView)
		{
			m_focusedView->setPosition(sf::Vector2i(m_focusedView->getPosition().x, m_focusedView->getPosition().y - 16));
			m_focusedView->update();
		}
	});

	KeyboardEvent* arrowRight = new  KeyboardEvent(sf::Keyboard::Right);
	arrowRight->setOnHoldCallback([&]()
	{
		if (m_focusedView)
		{
			m_focusedView->setPosition(sf::Vector2i(m_focusedView->getPosition().x + 16, m_focusedView->getPosition().y));
			m_focusedView->update();
		}
	});

	KeyboardEvent* arrowLeft = new  KeyboardEvent(sf::Keyboard::Left);
	arrowLeft->setOnHoldCallback([&]()
	{
		if (m_focusedView)
		{
			m_focusedView->setPosition(sf::Vector2i(m_focusedView->getPosition().x - 16, m_focusedView->getPosition().y));
			m_focusedView->update();
		}
	});
	m_eventHandle.addEvent(arrowUp);
	m_eventHandle.addEvent(arrowDown);
	m_eventHandle.addEvent(arrowLeft);
	m_eventHandle.addEvent(arrowRight);

	//gui============

	//tools

	pneTools = new GuiWindow("Tools", 550);
	addGuiRoot(pneTools);
	clhToolBox = new CollapsingHeader("ToolBox");
	clhGeneralVariables = new CollapsingHeader("General Variables");
	pneTools->getChildren().addAll(clhGeneralVariables, clhToolBox);


	//general variables 
	{
		View2D::setGridSize(16);
		sdrGridSize = new Pow2Slider("Grid Size", 1, 128, 16);
		sdrGridSize->setOnValueChange([&](int lastv, int newv)
		{
			View2D::setGridSize(newv);
			glvTopView->update();
			glvFrontView->update();
			glvLeftView->update();
		});

		HelpMarker* hpmGridSize = new HelpMarker("The snap size of the grid.\nIts recommended that you use a 2^n number as value, the plus/minus buttons automaticly do this. \n\nNOTE: do not confuse this for zoom levels.");
		hpmGridSize->setSameLine(true);

		clhGeneralVariables->getChildren().addAll(sdrGridSize, hpmGridSize);


		sdrGridZoom = new Pow2Slider("Zoom", 1, 1024, 1);
		sdrGridZoom->setOnValueChange([&](int lastv, int newv)
		{
			View2D::setGridZoom(newv);
			glvTopView->update();
			glvFrontView->update();
			glvLeftView->update();
		});

		clhGeneralVariables->getChildren().addAll(sdrGridZoom);
	}


	//map view

	pneMapView = new GuiWindow("Map View", 900);
	addGuiRoot(pneMapView);
	pneMapView->setOnResize([&](int w, int h)
	{
		//TODO: resize
	});

	glvTopView = new View2D(400, 400);
	glvFrontView = new View2D(400, 400);
	glvLeftView = new View2D(400, 400);
	glv3D = new View3D(400, 400);

	glvTopView->setSameLine(true);
	glvFrontView->setSameLine(true);

	glvTopView->setOnClickEvent([&]() { m_focusedView = glvTopView; });
	glvFrontView->setOnClickEvent([&]() { m_focusedView = glvFrontView; });
	glvLeftView->setOnClickEvent([&]() { m_focusedView = glvLeftView; });

	pneMapView->getChildren().addAll(glv3D, glvTopView, glvLeftView, glvFrontView);

	//MAINBAR
	mainMenu = new MainMenu();
	addGuiRoot(mainMenu);

	meiFile = new MenuList("File");
	meiEdit = new MenuList("Edit");
	meiViewDropdown = new ViewDropdown();
	meiViewDropdown->add(pneTools);
	meiViewDropdown->add(pneMapView);


	//TODO: SETTINGS, PREFORMANCE
	mainMenu->getChildren().addAll(meiFile, meiEdit, meiViewDropdown);
}

bool orb::editor::MainEditorState::isActive()
{
	return true;
}


void orb::editor::MainEditorState::preUpdate()
{
	ImGui::ShowDemoWindow();
}