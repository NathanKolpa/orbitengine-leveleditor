#pragma once
#include "../../Core/Structure/GameState.h"
#include "../../Core/ResourceLoader/TextureResource.h"

#include "../../Core/GUI/Panes/GuiWindow.h"
#include "../../Core/GUI/Nodes/CollapsingHeader.h"
#include "../../Core/GUI/Nodes/Button.h"
#include "../../Core/GUI/Nodes/SmallButton.h"
#include "../../Core/GUI/Nodes/IntSlider.h"
#include "../../Core/GUI/Nodes/MenuItem.h"
#include "../../Core/GUI/Nodes/HelpMarker.h"

#include "../GlViews/View3D.h"
#include "../GlViews/View2D.h"
#include "../Gui/ViewDropdown.h"
#include "../Gui/Pow2Slider.h"


#include "../../Core/GUI/Panes/MainMenu.h"

namespace orb
{
	namespace editor
	{
		class MainEditorState : public GameState
		{
		private:
			MainMenu* mainMenu;
			MenuList* meiFile;
			MenuList* meiEdit;
			ViewDropdown* meiViewDropdown;

			GuiWindow* pneTools;
			CollapsingHeader* clhGeneralVariables;
			CollapsingHeader* clhToolBox;

			Pow2Slider* sdrGridSize;
			Pow2Slider* sdrGridZoom;

			GuiWindow* pneMapView;

			View2D* glvTopView;
			View2D* glvFrontView;
			View2D* glvLeftView;
			View2D* m_focusedView;

			View3D* glv3D;

		public:
			MainEditorState(Window* window);
		private:
			bool isActive();
			void preUpdate();
		};
	}
}