#pragma once
#include "../../Core/GUI/Panes/MainMenu.h"
#include "../../Core/GUI/Panes/GuiWindow.h"
#include <vector>

namespace orb
{
	namespace editor
	{
		class ViewDropdown : public MenuList
		{
		private:
			std::vector<GuiWindow*> m_windows;
		public:
			ViewDropdown();
			void add(GuiWindow* window);
			void draw();
		};
	}
}