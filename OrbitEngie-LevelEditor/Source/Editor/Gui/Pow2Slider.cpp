#include "Pow2Slider.h"
#include "../../Core/Math/Functions.h"

orb::editor::Pow2Slider::Pow2Slider(std::string label, int min, int max, int start)
{
	islSlider = new IntSlider(label, min, max);
	btnMin = new Button(" - ");
	btnPlus = new Button(" + ");

	btnPlus->pushColor(ImGuiCol_Button, ImColor::HSV(0.3f, 0.6f, 0.6f));
	btnPlus->setOnAction([&]()
	{
		int curr = islSlider->getValue();
		int newValue = getNextPow2(curr);


		if (newValue <= islSlider->getMax())
			islSlider->setValue(newValue);
	});


	btnMin->pushColor(ImGuiCol_Button, ImColor::HSV(7.0f, 0.6f, 0.6f));
	btnMin->setOnAction([&]()
	{
		int curr = islSlider->getValue();
		int newValue = getPrevPow2(curr);


		if (newValue >= islSlider->getMin())
			islSlider->setValue(newValue);
	});

	btnPlus->setSameLine(true);
	islSlider->setSameLine(true);
	islSlider->setValue(start);

	m_children.addAll(btnMin, btnPlus, islSlider);
}

orb::editor::Pow2Slider::~Pow2Slider()
{
}

void orb::editor::Pow2Slider::setOnValueChange(std::function<void(int lastValue, int newValue)> lamda)
{
	islSlider->setOnValueChange(lamda);
}
void orb::editor::Pow2Slider::draw()
{
	m_children.drawAll();
}
