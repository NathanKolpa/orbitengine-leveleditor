#pragma once
#include "../../Core/GUI/Nodes/IntSlider.h"
#include "../../Core/GUI/Nodes/Button.h"
#include "../../Core/GUI/Children.h"

namespace orb
{
	namespace editor
	{
		class Pow2Slider : public GuiNode
		{
		private:
			Button* btnPlus;
			Button* btnMin;
			IntSlider* islSlider;
			Children<GuiNode> m_children;

		public:
			Pow2Slider(std::string label, int min, int max, int start = 0);
			~Pow2Slider();
		public:
			void setOnValueChange(std::function<void(int lastValue, int newValue)> lamda);
			void draw();
		};
	}
}