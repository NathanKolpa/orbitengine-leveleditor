#include "ViewDropdown.h"

orb::editor::ViewDropdown::ViewDropdown()
	:MenuList("View")
{
}


void orb::editor::ViewDropdown::add(GuiWindow * window)
{
	m_windows.push_back(window);
}

void orb::editor::ViewDropdown::draw()
{
	if (ImGui::BeginMenu("View"))
	{
		for (int i = 0; i < m_windows.size(); i++)
		{
			if (m_windows[i])
			{
				if (ImGui::MenuItem(m_windows[i]->getLabel().c_str(), NULL, m_windows[i]->isOpen()))
				{
					bool newState = !(m_windows[i]->isOpen());

					m_windows[i]->setEnable(newState);
				}
			}
		}
		ImGui::EndMenu();
	}
}
