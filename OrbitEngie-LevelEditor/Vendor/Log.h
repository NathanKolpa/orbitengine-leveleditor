#pragma once
#include <SFML/OpenGL.hpp>
#include <iostream>

#ifdef _DEBUG
/*
* Call this function with every opengl function
* This function Will check errors with every call
* This function Will output a error message when a error is detected
*/
#define ASSERT(x) if(!(x)) ErrorBreak();
#define GLfnc(x) ClearGLError(); x; ASSERT(GLLogCall(#x, __FILE__, __LINE__))

#else
/*
* For preformance sake it will not check errors when in release
*/
#define GLfnc(x) x;

#endif

static void ClearGLError()
{
	while (glGetError() != GL_NO_ERROR);
}

static void ErrorBreak()
{
	std::cout << "Would you like to exit the program? (y/n)" << std::endl;

	char ans;
	std::cin >> ans;

	if (ans == 'y' || ans == 'Y')
	{
		exit(-1);
	}
}

static bool GLLogCall(const char* Function, const char* File, int line)
{

	while (GLenum error = glGetError())
	{
		HANDLE ConsoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
		std::cout << std::endl;
		std::cout << "<===== OpenGL ERROR =====>" << std::endl;
		std::cout << "Error Code: " << error << std::endl;
		std::cout << "In file: " << File << std::endl;
		std::cout << "At: line " << line << std::endl;
		std::cout << "Called By: " << Function << ';' << std::endl;
		std::cout << "<===============================>" << std::endl;
		return false;
	}

	return true;

}